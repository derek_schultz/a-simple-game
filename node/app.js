var DEBUG = true;

var app = require('http').createServer(handler)
  , io = require('socket.io').listen(app)
  , fs = require('fs')
  , redis = require('redis')
  , client = redis.createClient()

app.listen(8005,"129.161.41.173");

client.on("error", function (err) {
  console.log("Error " + err);
});

function handler (req, res) {

}

var INITIAL_FUNDS = 10;
var NUM_PLAYERS = 4;

var Player = function(socket, ip, game){
  this.socket = socket;
  this.ip = ip;
  game.addPlayer(this);
}

var Game = function(){
  this.players = [];
  this.total_contributions = 0;
  this.round = 0;
  this.rounds = [];
}

var Round = function(){

}

Game.prototype.addPlayer = function(player) {
  this.players.push(player);
  if(this.players.length >= NUM_PLAYERS){
    startGame(this);
  }
}
Game.prototype.removePlayer = function(player) {
  for(var i = 0; i < this.players.length; i++){
    if(this.players[i] == player){
      this.players.splice(i,1);
      return true;
    }
  }
  return false; 
}
Game.prototype.allHaveContributed = function() {
  for(var i = 0; i < this.players.length; i++){
    if(!this.players[i].has_contributed)
      return false;
  }
  return true;
}
function startGame(game){
  var all_funds = [];
  for(var i = 0; i < game.players.length; i++){
    game.players[i].funds = INITIAL_FUNDS;
    game.players[i].has_contributed = false;
    all_funds.push(game.players[i].funds);
  }

  for(var i = 0; i < game.players.length; i++){
    game.players[i].socket.emit('new-game', 
    {
      player:i,
      players:game.players.length,
      round:0,
      funds:all_funds
    }
    );
    game.players[i].socket.on('contribute', function(data) {
      console.log(data);
      var amount_contributed = data["amount"];
      var this_game = find_game(this);
      var this_player = find_player(this);
      if(!((this_player.has_contributed)||(amount_contributed < 0)||(amount_contributed > this_player.funds))){
        this_player.amount_contributed = amount_contributed;
        this_player.funds -= amount_contributed;
        this_player.has_contributed = true;
        if(this_game.allHaveContributed() == true){
          startNextRound(this_game);
        }
      }
    });
  }
  startTimer(game,game.round);
  ongoing_games.push(next_game_to_fill);
  next_game_to_fill = new Game();
}
function startNextRound(game){
  console.log(game);
  var contributions = [];
  var total_contributions = 0;
  for(var i = 0; i < game.players.length; i++){
    contributions.push(game.players[i].amount_contributed);
    total_contributions += game.players[i].amount_contributed;
  }
  var each_receives = Math.floor(total_contributions*3.0/NUM_PLAYERS);
  for(var i = 0; i < game.players.length; i++){
    game.players[i].funds += each_receives;
  }
  var funds_list = [];
  for(var i = 0; i < game.players.length; i++){
    funds_list.push(game.players[i].funds);
  }
  // TODO with some probability end the game
  var lastRound = false;
  if(Math.random() <= game.round*.01)
    lastRound = true;
  var roundToArchive = new Round();
  roundToArchive.round = game.round;
  roundToArchive.funds = funds_list;
  roundToArchive.contributions = contributions;
  roundToArchive.eachReceives = each_receives;
  game.rounds.push(roundToArchive);

  for(var i = 0; i < game.players.length; i++){
    game.players[i].socket.emit('round-complete',
      {
        round: game.round,
        funds: funds_list,
        contributions: contributions,
        eachReceives: each_receives,
        lastRound: lastRound
      }
    );
  }
  if(lastRound){
    endGame(game);
    return;
  }
  game.round += 1;
  // reset player variables
  for(var i = 0; i < game.players.length; i++){
    game.players[i].has_contributed = false;
  }
  startTimer(game,game.round);
}
function startTimer(game,round){
  setTimeout(function(game,round) {
    if(game.round == round){
      for(i in ongoing_games){
        if(ongoing_games[i] == game){
          ongoing_games.splice(i,1);
          break;
        }
      }
      for(i in game.players){
        game.players[i].socket.emit('timeout-error');
      }
      delete game;
      console.log("DELETED GAME");
    }
  },45000,game,game.round);
}
function endGame(game){
  console.log("GAME END");
  // TODO : save game to database
  delete game;
}
function find_player(socket){
  for(var i = 0; i < ongoing_games.length; i++){
    var game = ongoing_games[i];
    for(var j = 0; j < game.players.length; j++){
      if(game.players[j].socket == socket){
        return game.players[j];
      }
    }
  }
}
function find_game(socket){
  for(var i = 0; i < ongoing_games.length; i++){
    var game = ongoing_games[i];
    for(var j = 0; j < game.players.length; j++){
      if(game.players[j].socket == socket){
        return game;
      }
    }
  }
}
var next_game_to_fill = new Game();
var ongoing_games = [];
var completed_games = [];

io.sockets.on('connection', function (socket) {
  socket.on('request-game', function (data) {
    var current_game = next_game_to_fill;
    var new_ip = socket.handshake.address["address"];
    console.log(new_ip);
    if(!DEBUG){
      for(var i = 0; i < current_game.players.length;i++){
        if(current_game.players[i].ip == new_ip){
          console.log("This IP is already playing in the next game");
          socket.emit('rejected');
          return;
        }
      }
    }
    var new_player = new Player(socket,new_ip,next_game_to_fill);
    console.log("Player found!");
  });
  socket.on('request-cancel', function (data) {
    var current_game = next_game_to_fill;
    var current_player;
    for(var i = 0; i < current_game.players.length;i++){
      if(current_game.players[i].socket == socket){
        current_player = current_game.players[i];
        break;
      }
    }
    console.log(next_game_to_fill.removePlayer(current_player));
  });
});

// chat stuff