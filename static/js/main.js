var SERVER = "http://129.161.41.173:8005"

game = new Game();
network = new Network(game, SERVER);

var Events = {
    requestGame: function (event) {
        network.requestGame();
    },
    cancelRequest: function (event) {
        network.cancelRequest();
    },
    submitContribution: function (event) {
        network.submitContribution();
        event.preventDefault();
        return false;
    }
}

$(document).ready(function () {

    $('#get-started-btn').click(Events.requestGame);
    $('.cancel-btn').click(Events.cancelRequest);
    $('#contribution-form').submit(Events.submitContribution);

    /* Disallow non-number entry in input type="number" */
    $('input[type=number]').keydown(function(event) {
        // Allow: backspace, delete, tab, escape, and enter
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
             // Allow: Ctrl and Meta sequences
            (event.ctrlKey === true || event.metaKey == true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });

    window.onbeforeunload = function() {
        return 'This action will end the current game.';
    };

});