var Network = function (game, server) {

    this.game = null;
    this.server = server;
    this.socket = io.connect(this.server);
    this.socket.network = this;

    this.socket.on('new-game', function (data) {
        this.network.game.startTimer();
        this.network.game.set('round', data.round + 1);
        this.network.game.set('player', data.player);
        this.network.game.set('funds', data.funds[this.network.game.player]);
        this.network.game.set('players', data.players);
        $('#welcome').hide();
        $('#board').show();
        $('#connecting').modal('hide');

        var opponentContributions = '';
        for (i = 0; i < data.players; i++) {
            if (i == this.network.game.player) {
                opponentContributions += '<th><span class="glyphicon glyphicon-user"></span> You</th>';
                $('.scoreboard table tr').first().append('<th><span class="glyphicon glyphicon-user"></span> You</th>');
            }
            else {
                opponentContributions += "<th>P"+ (i+1) +"</th>";
                $('.scoreboard table tr').first().append("<th>Opponent "+ (i+1) +"</th>");
            }
            $('.scoreboard table tr').last().append("<td>$"+data.funds[i]+"</td>");
        }
        $(opponentContributions).insertAfter('#bankroll-col');
    });

    this.socket.on('round-complete', function (data) {
        this.network.game.logRound(data);
        if (data.lastRound) {
            alert('TODO: end game screen');
        }
    });

    this.requestGame = function () {
        $('#connecting').modal();

        this.game = new Game();
        this.socket.emit('request-game');

    }

    this.cancelRequest = function () {
        this.socket.emit('request-cancel');
        $('#connecting').modal('hide');
    }

    this.submitContribution = function () {
        contribution = parseInt($('#contribution').val());
        if (isNaN(contribution)) {
            contribution = 0;
            $('#contribution').val('0');
        }
        if (contribution > this.game.funds) {
            alert("Please enter an amount you can afford.");
            return false;
        }
        $('#contribution').prop("disabled", true);
        $('#contribution-form button[type=submit]').prop("disabled", true)
            .html('<span class="glyphicon glyphicon-refresh"></span> Waiting...');
        this.socket.emit('contribute', { amount: contribution });
    }

}