var Game = function () {

    this.round = 0;
    this.rounds = [];

    this.set = function (key, value) {
        this[key] = value;
        $('.bound-value[data-variable='+key+']').html(value);
    }

    this.startTimer = function () {
        $('.timer .progress-bar').css('width', '100%')
            .removeClass('progress-bar-warning progress-bar-danger')
            .addClass('progress-bar-success')
            .animate({'width': '0%'}, {'duration': 30 * 1000, 'easing': 'linear'});
        this.yellowTimeout = setTimeout(this.timerSwitchYellow, 15 * 1000);
        this.redTimeout = setTimeout(this.timerSwitchRed, 25 * 1000);
        this.sendTimeout = setTimeout(this.timerSend, 30 * 1000);
    }

    this.timerSwitchYellow = function () {
        $('.timer .progress-bar').removeClass('progress-bar-success').addClass('progress-bar-warning');
    }

    this.timerSwitchRed = function () {
        $('.timer .progress-bar').removeClass('progress-bar-warning').addClass('progress-bar-danger');
    }

    this.timerSend = function () {
        $('#contribution-form').submit();
    }

    this.logRound = function (data) {
        clearTimeout(this.yellowTimeout);
        clearTimeout(this.redTimeout);
        clearTimeout(this.sendTimeout);
        this.startTimer();
        var round = [this.round, this.funds];
        round = round.concat(data.contributions);
        round.push(data.eachReceives);
        round.push(data.funds[this.player]);
        this.set('round', data.round + 2);
        this.set('funds', data.funds[this.player]);
        this.rounds.push(round);

        var tds = $('.scoreboard table tr').last().find('td');
        for (i = 0; i < this.players; i++) {
            tds.eq(i).html("$"+data.funds[i]);
        }

        var prevRoundsHeader = $('.previous-rounds table tr').first();
        $("<tr></tr>").insertAfter(prevRoundsHeader);
        for (stat in round) {
            $('.previous-rounds table tr').first().next().append("<td>"+round[stat]+"</td>");
        }

        $('#contribution').prop("disabled", false).val('');
        $('#contribution-form button[type=submit]').prop("disabled", false)
            .html('<span class="glyphicon glyphicon-ok"></span> Submit');

    }

}